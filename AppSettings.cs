using System;

namespace RemoteHelperClient
{
    class AppSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public long Deadline { get; set; }
    }
}