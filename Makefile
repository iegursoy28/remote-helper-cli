all:
	dotnet restore
	dotnet build -c Release

release: all
	dotnet publish -c Release -r linux-x64
	dotnet publish -c Release -r linux-arm
	dotnet publish -c Release -r win-x64

clean:
	dotnet clean
	rm -rf bin obj