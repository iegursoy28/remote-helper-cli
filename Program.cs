﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CommandLine;
using Grpc.Core;

namespace RemoteHelperClient
{
    class Program
    {
        private static readonly string _app_config_name = "config.json";

        public class Options
        {
            [Option('s', "setting", Required = false, HelpText = "Set server config host port.")]
            public bool IsSet { get; set; }

            [Option('h', "host", Required = false, Default = "0.0.0.0", HelpText = "Input host.")]
            public string Host { get; set; }

            [Option('p', "port", Required = false, Default = 7777, HelpText = "Input port.")]
            public int Port { get; set; }

            [Option('l', "list", Required = false, HelpText = "List files.")]
            public bool IsList { get; set; }

            [Option('g', "get", Required = false, HelpText = "Get file.")]
            public bool IsGet { get; set; }

            [Option('i', "get-file-name", Required = false, Default = "", HelpText = "Get file name to will be download.")]
            public string GetFileName { get; set; }

            [Option('s', "push", Required = false, HelpText = "Push file.")]
            public bool IsPush { get; set; }

            [Option('f', "push-file-path", Required = false, Default = "", HelpText = "Push file path to will be push.")]
            public string PushFilePath { get; set; }

            [Option('r', "run", Required = false, HelpText = "Run CMD.")]
            public bool IsRunCmd { get; set; }

            [Option('c', "run-cmd", Required = false, Default = "", HelpText = "Console commands to will be run.")]
            public string RunCmd { get; set; }
        }

        public class RemoteHelperClient : IDisposable
        {
            private Channel channel;
            private Remotehelper.ServerService.ServerServiceClient client;

            public RemoteHelperClient(string host, int port)
            {
                try
                {
                    channel = new Channel(host + ":" + port, ChannelCredentials.Insecure);
                    client = new Remotehelper.ServerService.ServerServiceClient(channel);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e);
                }
            }

            public Remotehelper.ServerService.ServerServiceClient GetClient()
            {
                return client;
            }

            public async Task ls()
            {
                try
                {
                    using (var call = client.ListFiles(new Remotehelper.EmptyQ() { }))
                    {
                        var resp = call.ResponseStream;
                        while (await resp.MoveNext())
                        {
                            var c = resp.Current;
                            System.Console.WriteLine($"{c.Id,5} {(c.IsDir ? "DIR" : "FILE"),10} {c.FileSize,10} {c.Path,20} {c.Name,20}");
                        }
                    }
                }
                catch (RpcException e) { System.Console.WriteLine(e); }
            }

            public async Task get(string fileName)
            {
                using (var call = client.Download(new Remotehelper.DownloadRequest() { RemoteName = fileName }))
                {
                    if (!await call.ResponseStream.MoveNext())
                    {
                        System.Console.WriteLine("Error get file");
                        return;
                    }

                    using (var stream = File.OpenWrite(fileName))
                    {
                        do
                        {
                            var bytes = call.ResponseStream.Current.Context.ToByteArray();
                            stream.Write(bytes, 0, bytes.Length);
                        } while (await call.ResponseStream.MoveNext());

                        System.Console.WriteLine($"Download finish: {fileName}");
                    }
                }
            }

            public async Task push(string pushFilePath)
            {
                using (var call = client.Upload())
                {
                    using (Stream source = File.OpenRead(pushFilePath))
                    {
                        byte[] buffer = new byte[1024 * 1024 * 2];
                        int bytesRead;
                        while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            System.Console.WriteLine($"Upload status: %{(((float)source.Position / (float)source.Length) * 100.0f):F2}");
                            await call.RequestStream.WriteAsync(new Remotehelper.UploadRequest
                            {
                                RemoteName = pushFilePath,
                                Context = Google.Protobuf.ByteString.CopyFrom(buffer, 0, bytesRead)
                            });
                        }

                        await call.RequestStream.CompleteAsync();
                    }
                }
            }

            public async Task runCMD(string cmd)
            {
                var resp = await client.RunSingleCommandAsync(new Remotehelper.CommandRunRequest { CommandName = cmd });
                System.Console.WriteLine($"CMD result:\n{resp.Stdout}");
            }

            public void Dispose()
            {
                if (channel != null)
                {
                    try { channel.ShutdownAsync(); } catch { }
                    System.Console.WriteLine("Shutdown client.");
                }
            }
        }

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
           .WithParsed<Options>(o => RunWithOptsAsync(o))
           .WithNotParsed<Options>(e => ErrorHandle(e));
        }

        static void ErrorHandle(IEnumerable<Error> e)
        {
            foreach (var item in e)
                System.Console.WriteLine(item.StopsProcessing);
        }

        static void RunWithOptsAsync(Options o)
        {
            if (!System.IO.File.Exists(_app_config_name))
                System.IO.File.WriteAllText(_app_config_name, "{\"Host\":\"0.0.0.0\",\"Port\":7777,\"Deadline\":500}");

            AppSettings appSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<AppSettings>(System.IO.File.ReadAllText(_app_config_name));

            if (o.IsSet)
            {

                if (appSettings == null)
                {
                    System.Console.WriteLine("Null app settings");
                    return;
                }

                if (o.Host != null && o.Host.Length > 0)
                    appSettings.Host = o.Host;

                if (o.Port > 0)
                    appSettings.Port = o.Port;

                System.IO.File.WriteAllText(_app_config_name, Newtonsoft.Json.JsonConvert.SerializeObject(appSettings));
            }
            else
            {
                if (o.IsList)
                {
                    using (var context = new RemoteHelperClient(appSettings.Host, appSettings.Port))
                    {
                        context.ls().Wait();
                    }
                }
                else if (o.IsGet)
                {
                    if (o.GetFileName.Length <= 0)
                    {
                        System.Console.WriteLine("please input file name");
                        return;
                    }

                    using (var context = new RemoteHelperClient(appSettings.Host, appSettings.Port))
                    {
                        context.get(o.GetFileName).Wait();
                    }
                }
                else if (o.IsPush)
                {
                    if (o.PushFilePath.Length <= 0)
                    {
                        System.Console.WriteLine("please input file full path");
                        return;
                    }

                    using (var context = new RemoteHelperClient(appSettings.Host, appSettings.Port))
                    {
                        context.push(o.PushFilePath).Wait();
                    }
                }
                else if (o.IsRunCmd)
                {
                    if (o.RunCmd.Length <= 0)
                    {
                        System.Console.WriteLine("please input run cmd");
                        return;
                    }

                    using (var context = new RemoteHelperClient(appSettings.Host, appSettings.Port))
                    {
                        context.runCMD(o.RunCmd).Wait();
                    }
                }
            }
        }
    }
}
